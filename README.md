# python-crowd

A simple demonstration of using [Crowd](https://www.atlassian.com/software/crowd/overview/)'s [REST API](https://developer.atlassian.com/display/CROWDDEV/Crowd+REST+APIs)
to authenticate a user by username and password
and also to validate them by SSO token.
