#!/usr/bin/python

# Demonstrate using Crowd's REST API to create
#  a session with a username and password and then validate
#  it, as we would do on a subsequent request

from __future__ import print_function

import httplib2
import json
import sys

# Crowd server configuration
url = 'http://localhost:8095/crowd'
crowdAppName = 'appname'
crowdAppPassword = 'XXXX'

# The user to attempt to login as
username = 'user'
password = 'user'
remote_address = '127.0.0.1'


def event(s):
  print("----")
  print("    " + s)
  print("----")

url += '/rest/usermanagement/1'

h = httplib2.Http()
h.add_credentials(crowdAppName, crowdAppPassword)


event("Starting")

# Perform a trivial search, to confirm that the REST API
#  is working for our application
testU = url + '/search?entity-type=user&max-results=1'
data = '<null-search-restriction/>'

headers = {
 'Content-Type': 'application/xml',
 'Accept': 'application/json'
}

resp, content = h.request(testU, 'POST', body=data, headers=headers)
if resp.status != 200:
  print(resp)
  sys.exit(10)

print('Content:')
print(content)

c = json.loads(content.decode('utf-8'))
print(c)

for u in c['users']:
  print(u['name'])

event("Crowd API found")


authContext = {
  'username': username,
  'password': password,
  'validation-factors': {
    'validationFactors': [
      {
        'name': 'remote_address',
        'value': remote_address
      },

# If the request came via a proxy then set these validation factors
# instead, so Crowd can determine the originating address.
#      {
#        'name': 'remote_address',
#        'value': proxy_address
#      },
#      {
#        'name': 'X-Forwarded-For',
#        'value': '127.0.0.1'
#      }
    ]
  }
}


# Create a Crowd session for one of our users

# loginU = url + '/authentication?username=user'
loginU = url + '/session'

headers = {
 'Content-Type': 'application/json',
 'Accept': 'application/json'
}

print(json.dumps(authContext))

token = None

resp, content = h.request(loginU, 'POST', body=json.dumps(authContext), headers=headers)

if resp.status != 201:
  print(resp)
  sys.exit(10)

print(content)

c = json.loads(content.decode('utf-8'))

token = c['token']
print(token)
print(c['link']['href'])

event("SSO session created for user")

# We could use that token with Curl
# curl -I --cookie crowd.token_key=XXX http://127.0.0.1:8095/crowd/console/


# Validate a user with a Crowd session token that they
#  provide
tokenU = url + '/session/' + token

vf = authContext['validation-factors']

# Use different validationFactors and the session check will fail
# vf = {'validationFactors': []}

print(json.dumps(vf))

resp, content = h.request(tokenU, 'POST', body=json.dumps(vf), headers=headers)

if resp.status != 200:
  print(resp)
  sys.exit(10)

print(json.loads(content.decode('utf-8')))
print(json.loads(content.decode('utf-8'))['user']['name'])

event("Session validated from token")


# Delete the session when the user logs out of SSO
resp, content = h.request(tokenU, 'DELETE')

print(resp.status)

if resp.status != 204:
  print(resp)
  sys.exit(10)

event("User session destroyed")
