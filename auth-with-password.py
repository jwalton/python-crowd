#!/usr/bin/python

# Use Crowd's REST API for simple password authentication.

from __future__ import print_function

import httplib2
import json
import sys
try:
  from urllib.parse import quote
except ImportError:
  from urllib import quote

# Crowd server configuration
url = 'http://localhost:8095/crowd'
crowdAppName = 'appname'
crowdAppPassword = 'XXXX'

# The user to attempt to login as
username = 'user'
password = 'user'

url += '/rest/usermanagement/1'

h = httplib2.Http()
h.add_credentials(crowdAppName, crowdAppPassword)


authUrl = url + '/authentication?username=' + quote(username)


headers = {
 'Content-Type': 'application/json',
 'Accept': 'application/json'
}

resp, content = h.request(authUrl, 'POST', body=json.dumps({'value': password}), headers=headers)

if resp.status == 200:
  print('Okay.')
  sys.exit(0)
elif resp.status == 400:
  print('Failed to authenticate.')
  sys.exit(10)
else:
  print('Unexpected response: %s' % resp)
  print(content)
  sys.exit(10)
